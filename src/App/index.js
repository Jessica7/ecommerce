import React from 'react';
import Header from '../components/Header';
import './style.sass';

const App = () => {
  return <div className="main">
    <div className="wrapper">
      <div className="wrapper-content"> Hello World!</div>
      <Header />
    </div>
  </div>;
};

export default App;
