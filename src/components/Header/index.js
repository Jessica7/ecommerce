import React from 'react';
import SubHeader from './SubHeader';
import styles from './style';
import logoURL from './images/react-logo.svg';

const Header = () => {
  return <header className="main">
    <img className="logo" src={logoURL} height="125"/>

    <div className="wrap">
      <h1 className="title">Search</h1>
      <SubHeader>A Simple Search page</SubHeader>
    </div>
  </header>;
};

Header.displayName = 'Header';

export default Header;
